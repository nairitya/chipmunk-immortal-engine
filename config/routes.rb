Rails.application.routes.draw do
  root 'home#index'

  #Signup part
  post "/signup" => "signup#postsignup"

  #Login part
  post "/login" => "login#login"

  #Logput part
  get "/logout" => "login#logout"

  #User details
  get "/getuser" => "service#getuser"

  #Friend manager!
  post "/addfriend" => "service#addfriend"
  get "/getfriendlist" => "service#pendinggetfriendlist"
  post "/acceptfriendrequest" => "service#acceptfriendrequest"
  post "/declinereq" => "service#declinefriendrequest"

  #account confirmation!
  get "/user/confirm/:id/:code" => "signup#confirm"

  #modify user details !
  put "/user/modify" => "service#modify_user_detail"
  put "/user/updatepassword" => "service#password_update"

  #Get List of all friends !
  get "/user/listfriends" => "service#listallfriends"

  #Add a country
  post "/country" => "country#add_country"
  #Get the coutnry list
  get "/country" => "helper#get_country"
  #Search a coutry
  get "/country/search" => "helper#look_country"

  #Add a city
  post "/city" => "country#add_city"
  #Get the city list
  get "/city" => "helper#get_city"
  #Search a city
  get "/city/search" => "helper#look_city"

  #Add a hotel
  post "/hotel" => "country#add_hotel"
  #Get the restaurant of a city
  get "/restaurant" => "helper#get_restaurant"
  #Search a restaurant in a city
  get "/restaurant/search" => "helper#look_restaurant"

  #Get the bar of a city
  get "/bar" => "helper#get_bar"
  #Search a bar in a city
  get "/bar/search" => "helper#look_bar"


  #Add a review to the restaurant
  post "/hotel/review" => "review#add_hotel_review"
  #Get the reviews of the restaurant
  get "/hotel/review" => "review#get_restaurant_review"

  #Add a review to the bar
  post "/bar/review" => "review#add_bar_review"
  #Get the reviews of the restaurant
  get "/bar/review" => "review#get_bar_review"

end
