class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string :name
      t.string :lead_actor
      t.string :lead_actress
      t.integer :releasing_year
      t.string :photo
      t.string :desc
      t.string :budget
      t.string :gross
      t.string :tag

      t.timestamps
    end
  end
end
