class CreateFriends < ActiveRecord::Migration
  def change
    create_table :friends do |t|
      t.boolean :status
      t.integer :user_id_1
      t.integer :user_id_2

      t.timestamps
    end
  end
end
