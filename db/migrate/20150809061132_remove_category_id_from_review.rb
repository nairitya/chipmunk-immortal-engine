class RemoveCategoryIdFromReview < ActiveRecord::Migration
  def change
  	remove_column :reviews, :category_id
  end
end
