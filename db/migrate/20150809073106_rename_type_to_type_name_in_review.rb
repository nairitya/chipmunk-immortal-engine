class RenameTypeToTypeNameInReview < ActiveRecord::Migration
  def change
  	rename_column("reviews","type","type_name")
  end
end
