class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :description
      t.integer :category_id
      t.integer :star
      t.integer :count_up

      t.timestamps
    end
  end
end
