class CreateHotels < ActiveRecord::Migration
  def change
    create_table :hotels do |t|
      t.integer :city_id
      t.string :name
      t.string :address
      t.string :lat
      t.string :lang
      t.string :photos
      t.string :contact
      t.integer :is_restaurant
      t.integer :is_bar
      t.integer :is_hotel

      t.timestamps
    end
  end
end
