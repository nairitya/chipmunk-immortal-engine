class CountryController < ApplicationController
	before_filter :is_user_admin

	def add_country
		begin  
			data = ActiveSupport::JSON.decode(request.body.string)
		rescue JSON::ParserError  
			return render text:"Unable to parse the data"
		end

		c_name = data["name"].to_s
		c_code = data["code"].to_s

		country = Country.new(:name => c_name, :code => c_code)
		if(country.save)
			return render text: "true"
		else
			return render text: country.errors.full_messages
		end
	end

	def add_city
		begin  
			data = ActiveSupport::JSON.decode(request.body.string)
		rescue JSON::ParserError  
			return render text:"Unable to parse the data"
		end

		c_id = data["country_id"].to_s
		c_name = data["name"].to_s
		c_lat = data["lat"].to_s
		c_lang = data["lang"].to_s
		c_pic = data["pic"].to_s

		country = Country.where(:id => c_id).first
		if !country
			return render text: "This coutnry is not present right now!!"
		end

		city = City.new(:country_id => c_id, :name => c_name, :lat => c_lat, :lang => c_lang, :pic => c_pic)
		if(city.save)
			return render text: "true"
		else
			return render text: city.errors.full_messages
		end
	end

	def add_hotel
		begin  
			data = ActiveSupport::JSON.decode(request.body.string)
		rescue JSON::ParserError  
			return render text:"Unable to parse the data"
		end

		c_id = data["city_id"].to_s
		c_address = data["address"].to_s
		c_contact = data["contact"].to_s
		c_name = data["name"].to_s
		c_lat = data["lat"].to_s
		c_lang = data["lang"].to_s
		c_pic = data["pic"].to_s

		is_restaurant = data["is_restaurant"].to_s
		is_bar = data["is_bar"].to_s
		is_hotel = data["is_hotel"].to_s

		city = City.where(:id => c_id).first
		if !city
			return render text: "This city is not present right now!!"
		end

		hotel = Hotel.new(:city_id => c_id, :name => c_name, :lat => c_lat, :lang => c_lang, :photos => c_pic, :contact => c_contact, :address => c_address, :is_restaurant => is_restaurant, :is_bar => is_bar, :is_hotel => is_hotel)
		if(hotel.save)
			return render json: hotel
		else
			return render text: hotel.errors.full_messages
		end
	end

	# def add_bar
	# 	begin  
	# 		data = ActiveSupport::JSON.decode(request.body.string)
	# 	rescue JSON::ParserError  
	# 		return render text:"Unable to parse the data"
	# 	end

	# 	c_id = data["city_id"].to_s
	# 	c_address = data["address"].to_s
	# 	c_contact = data["contact"].to_s
	# 	c_name = data["name"].to_s
	# 	c_lat = data["lat"].to_s
	# 	c_lang = data["lang"].to_s
	# 	c_pic = data["pic"].to_s

	# 	city = City.where(:id => c_id).first
	# 	if !city
	# 		return render text: "This city is not present right now!!"
	# 	end

	# 	bar = Bar.new(:city_id => c_id, :name => c_name, :lat => c_lat, :lang => c_lang, :photos => c_pic, :contact => c_contact, :address => c_address)
	# 	if(bar.save)
	# 		return render json: bar
	# 	else
	# 		return render text: bar.errors.full_messages
	# 	end
	# end
end
