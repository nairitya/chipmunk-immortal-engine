class SignupController < ApplicationController

	before_filter :not_signed_in

	def postsignup
		begin  
			data = ActiveSupport::JSON.decode(request.body.string)
		rescue JSON::ParserError  
			return render text:"Unable to parse the data"
		end

		@fname = data["firstname"].to_s
		@lname = data["lastname"].to_s
		@uname = data["username"].to_s
		@pwd = data["password"].to_s
		@email = data["email"].to_s

		@user = User.new(:firstname => @fname, :lastname => @lname, :email => @email, :username => @uname.downcase, :password => @pwd)

		if(@user.save)
			key = SecureRandom.hex(16)
			confirm = Confirmation.new(:user_id => @user.id, :confirmation_code => key)

			if (confirm.save)
				EmailVerification.send_simple_message(key, @user.id, @user.email, "#{@user.firstname} #{@user.lastname}")
				render text: "true"
			else
				render text: confirm.errors.full_messages
			end

		else
			puts "#{@user.errors.to_json}"
			render text: @user.errors.full_messages
		end
	end

	def confirm
		@user = User.where(:id => params[:id], :confirmed => false).first
		if(@user)
			confirm = Confirmation.where(:user_id => params[:id], :confirmation_code => params[:code]).first
			if(confirm)
				User.where(:id => @user.id).limit(1).update_all(:confirmed => true)
				Confirmation.delete(confirm.id)
				render text: "true"
			else
				render text: "False Level-2"
			end
		else
			render text: "False Level-1"
		end
	end

end
