class ServiceController < ApplicationController
	before_filter :is_user_loggedin

	def getuser
		##TODO: Enhance the search using ElasticSearch.

		@starts_with = params["starts_with"]
		#Finally using desi querying style
		#Query below might be vulnurable to SQL Injection attacks.
		@users = User.select(:firstname, :lastname, :email, :id).where("firstname ilike '#{@starts_with}%' or lastname ilike '#{@starts_with}%' or email ilike '#{@starts_with}%'")
		render json: @users
	end

	def modify_user_detail

		begin  
			data = ActiveSupport::JSON.decode(request.body.string)
		rescue JSON::ParserError  
			return render text:"Unable to parse the data"
		end

		firstname = data["firstname"]
		lastname = data["lastname"]

		if ( firstname.to_s.nil? || firstname.to_s.empty? ) && ( lastname.to_s.empty? || lastname.to_s.nil?)
			render text: "Wrong Query !!"
			return
		elsif (firstname.to_s.nil? || firstname.to_s.empty? )
			users = User.where(:id => @user_id).limit(1).update_all(:lastname => lastname)
		elsif (lastname.to_s.empty? || lastname.to_s.nil?)
			users = User.where(:id => @user_id).limit(1).update_all(:firstname => firstname)
		else
			users = User.where(:id => @user_id).limit(1).update_all(:firstname => firstname, :lastname => lastname)
		end

		if(users)
			render text: "true"
		else
			render text: users.errors.full_messages
		end

		return
	end

	def password_update

		begin  
			data = ActiveSupport::JSON.decode(request.body.string)
		rescue JSON::ParserError  
			return render text:"Unable to parse the data"
		end

		current_password = data["current_password"].to_s
		new_password = data["new_password"].to_s

		user = User.where(:id => @user_id).first
		if user.try(:authenticate, current_password)
			user.password = new_password
		else
			render text: "Wrong password !!"
			return
		end

		if user.save
			render text: "true"
		else
			render text: user.errors.full_messages
		end
	end

	def lost_password
	end

	def addfriend
		begin  
			data = ActiveSupport::JSON.decode(request.body.string)
		rescue JSON::ParserError  
			return render text:"Unable to parse the data"
		end

		id = data["id"]

		if (id == @user_id.to_s)
			render text: "['You cannot send req to self']"
			return
		# else
		# 	Rails.logger.info "id=#{id} and @user_id=#{@user_id}"
		end
		
		temp_user = User.select(:id).where(:id => id).first
		if(!temp_user)
			render text:"['No such user exists!!']"
			return
		end
		
		query = "user_id_1=#{@user_id} and user_id_2=#{id} or user_id_1=#{id} and user_id_2=#{@user_id}"
		
		#check if he's already a friend <-- will never encounter this case.
			#If yes, <-- return error
			#Else <-- send the friend req !!
		# print query

		friends = Friend.where(query).first
		if(friends)
			if(friends.status == 1)
				render text: "['You are already friend with this person']"
				return
			else
				if(friends.user_id_1 == @user_id_1)
					render text: "['You already have sent him a friend req']"
					return
				else
					render text: "['You have a pending friend req from this person']"
					return
				end
			end
			return
		end

		#user_id_1 is always the person who sends the request.
		friend = Friend.new(:user_id_1 => @user_id, :user_id_2 => id, :status => 0)
		if(friend.save)
			render text: "true"
		else
			render text: friend.errors.full_messages
		end

		return
	end

	def pendinggetfriendlist

		page = params["page"]

		if(page == "0")
			page = 1
		end

		status = false
		friends = Friend.select(:user_id_1).where(:user_id_2 => @user_id, :status => status).paginate(:page => page, :per_page => 10).order('created_at DESC')

		user = Array.new
		friends.each do |fr|
			user.push(fr.user_id_1)
		end

		puts user
		if(user.empty?)
			render text: "['No friend request!!']"
			return
		else
			users = User.select(:firstname, :lastname, :id).find(user)
			render json: users
			return
		end

	end

	def acceptfriendrequest

		begin  
			data = ActiveSupport::JSON.decode(request.body.string)
		rescue JSON::ParserError  
			return render text:"Unable to parse the data"
		end

		id = data["id"]
		friends = Friend.where(:user_id_1 => id, :user_id_2 => @user_id, :status => false).first
		
		if(friends)
			friend = Friend.where(:id => friends.id).limit(1).update_all(status: true)
			if(friend)
				render text: "true"
				return
			else
				render text: "false"
				return
			end

		else
			render text: "You cannot accept this frind request !!"
			return
		end
		return
	end

	def declinefriendrequest

		begin  
			data = ActiveSupport::JSON.decode(request.body.string)
		rescue JSON::ParserError  
			return render text:"Unable to parse the data"
		end

		id = data["id"]
		friends = Friend.where(:user_id_1 => id, :user_id_2 => @user_id, :status => false).first
		
		if(friends)
			friend = Friend.delete(friends.id)
			if(friend)
				render text: "true"
				return
			else
				render text: "false"
				return
			end

		else
			render text: "No such friend request !!"
			return
		end
	end

	def listallfriends

		page = params["page"]

		if(page == "0")
			page = 1
		end
		status = true

		# query = "select user_id_1 as id from friends where user_id_2=#{@user_id} and status=#{status} UNION select user_id_2 as id from friends where user_id_1=#{@user_id} and status=#{status}"
		# @friends = Friend.find_by_sql(query)

		f1 = Friend.select("user_id_1 AS id, created_at").where(:user_id_2 => @user_id, :status => status)
		f2 = Friend.select("user_id_2 AS id, created_at").where(:user_id_1 => @user_id, :status => status)
		friends = Friend.from("(#{f1.to_sql} UNION #{f2.to_sql}) as friends").paginate(:page => page, :per_page => 10).order('created_at DESC')

		# @friends = Friend.select(:user_id_1).where(:user_id_2 => @user_id).paginate(:page => page, :per_page => 10).order('created_at DESC')
		

		user = Array.new
		friends.each do |fr|
			user.push(fr.id)
		end

		puts user
		if(user.empty?)
			render text: "['No friends!!']"
			return
		else
			users = User.select(:firstname, :lastname, :id).find(user)
			render json: users
			return
		end

	end
end
