class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  # Coming from only Mobile App. Hence no CSRF forgery :). A relief. 

	def initialize
		@redis = Redis.new(:host => "localhost", :password => "b840fc02d524045429941cc15f59e41cb7be6c52")
	end

	#True if user is loggedin. false otherwise
	def user_in
		if request.headers["Authorization"]
			if(get_value(request.headers["Authorization"]))
				return true
			else
				return false
			end
		else
			return false
		end
	end

	def is_user_loggedin
		user_detail = get_value(request.headers["Authorization"])
		if (user_detail)
			@user_id = user_detail.split(" ")[0]
			@user_confirmed = user_detail.split(" ")[1]
			return true
		else
			return render text: "Not logged in"
		end
	end

	#User is admin
	def is_user_admin
		if request.headers["Authorization"] == "IAmTheMofoAdmin"
			return true
		else
			return render text: "Not allowed"
		end
	end

	#Function to check whether user is logged in.
	def not_signed_in
		if(get_value(request.headers["Authorization"]))
			return render text: "Already logged in"
		else
			return true
		end
	end

	###
	# Redis functions to make it universal.
	# Single connection for all set/get/del
	###
	def set_key(key, value)
		@redis.set(key, value)
	end

	def get_value(key)
		return @redis.get(key)
	end

	def un_set(key)
		return @redis.del(key)
	end
end
