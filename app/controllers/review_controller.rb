class ReviewController < ApplicationController
	before_filter :is_user_loggedin

	def add_hotel_review
		begin  
			data = ActiveSupport::JSON.decode(request.body.string)
		rescue JSON::ParserError  
			return render text:"Unable to parse the data"
		end

		r_id = data["hotel_id"].to_s
		r_desc = data["description"].to_s
		r_star = data["star"].to_s
		r_count = 0
		r_user = @user_id

		restaurant = Hotel.where(:id => r_id).first
		if !restaurant
			return render :text => "This hotel is not present right now!!", :status => 400
		end

		dup_review = Review.where(:user_id => r_user, :type_id => r_id).first
		if dup_review
			return render :text => "Your review to this hotel has already been added!!", :status => 400
		end

		review = Review.new(:description => r_desc, :star => r_star, :count_up => r_count, :user_id => r_user, :type_id => r_id, :type_name => "HOTEL")

		if(review.save)
			return render :json => review, :status => 200	
		else
			return render :text => review.errors.full_messages, :status => 400
		end
	end

	#Add user here too <= TODO
	def get_restaurant_review

		r_id = params["hotel_id"]
		page = params["page"]

		restaurant = Hotel.where(:id => r_id).first
		if !restaurant
			return render :text => "This hotel is not present right now!!", :status => 400
		end

		review = Review.select(:id, :description, :count_up, :star, "type_id AS hotel_id").where(:id => r_id).paginate(:page => page, :per_page => 10).order("count_up DESC")
		return render :json => review, :status => 200
	end


	# def add_bar_review
	# 	begin  
	# 		data = ActiveSupport::JSON.decode(request.body.string)
	# 	rescue JSON::ParserError  
	# 		return render text:"Unable to parse the data"
	# 	end

	# 	r_id = data["bar_id"].to_s
	# 	r_desc = data["description"].to_s
	# 	r_star = data["star"].to_s
	# 	r_count = 0
	# 	r_type = "BAR"
	# 	r_user = @user_id

	# 	bar = Bar.where(:id => r_id).first
	# 	if !bar
	# 		return render :text => "This bar is not present right now!!", :status => 400
	# 	end

	# 	dup_review = Review.where(:user_id => r_user, :type_id => r_id).first
	# 	if dup_review
	# 		return render :text => "Your review to this bar has already been added!!", :status => 400
	# 	end

	# 	review = Review.new(:description => r_desc, :star => r_star, :count_up => r_count, :user_id => r_user, :type_id => r_id, :type_name => r_type)

	# 	if(review.save)
	# 		return render :json => review, :status => 200	
	# 	else
	# 		return render :text => review.errors.full_messages, :status => 400
	# 	end
	# end

	# def get_bar_review

	# 	r_id = params["bar_id"]
	# 	page = params["page"]
	# 	bar = Bar.where(:id => r_id).first
	# 	if !bar
	# 		return render :text => "This bar is not present right now!!", :status => 400
	# 	end

	# 	review = Review.select(:id, :description, :count_up, :star, "type_id AS bar_id").where(:id => r_id).paginate(:page => page, :per_page => 10).order("count_up DESC")
	# 	return render :json => review, :status => 200
	# end
end
