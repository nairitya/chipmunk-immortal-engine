class HelperController < ApplicationController
	#PS: SQL Injection possible in all where queries. Use ORM model instead.
	#Creating a MVP

	def get_country
		page = params["page"]
		if(page == "0")
			page = 1
		end
		
		countries = Country.select(:name, :code, :id).paginate(:page => page, :per_page => 10).order('name DESC')
		render json: countries
	end

	def look_country
		name = params["name"]
		countries = Country.select(:name, :code, :id).where("name ilike '#{name}%' or code ilike '#{name}%'").paginate(:page => 1, :per_page => 10).order('name DESC')
		render json: countries
	end


	def get_city
		page = params["page"]
		if(page == "0")
			page = 1
		end
		
		#One more condition here on coutnry!!
		cities = City.select(:name, :pic, :id).paginate(:page => page, :per_page => 10).order('name DESC')
		render json: cities
	end

	def look_city
		name = params["name"]
		cities = City.select(:name, :pic, :id).where("name ilike '#{name}%'").paginate(:page => 1, :per_page => 10).order('name DESC')
		render json: cities
	end

	def get_restaurant
		page = params["page"]
		c_id = params["city_id"]
		if(page == "0")
			page = 1
		end
		
		restaurants = Hotel.select(:name, :photos, :id, :lat, :lang, :contact, :address).where(:city_id => c_id, :is_restaurant => 1).paginate(:page => page, :per_page => 10).order('name DESC')
		render json: restaurants
	end

	def look_restaurant
		name = params["name"]
		c_id = params["city_id"]
		restaurants = Hotel.select(:name, :photos, :id, :lat, :lang, :contact, :address).where("is_restaurant=1 and city_id = #{c_id} and name ilike '#{name}%'").paginate(:page => 1, :per_page => 10).order('name DESC')
		render json: restaurants
	end

	def get_bar
		page = params["page"]
		c_id = params["city_id"]
		if(page == "0")
			page = 1
		end
		
		bars = Hotel.select(:name, :photos, :id, :lat, :lang, :contact, :address).where(:city_id => c_id, :is_bar => 1).paginate(:page => page, :per_page => 10).order('name DESC')
		render json: bars
	end

	def look_bar
		name = params["name"]
		c_id = params["city_id"]
		bars = Hotel.select(:name, :photos, :id, :lat, :lang, :contact, :address).where("is_bar=1 and city_id = #{c_id} and name ilike '#{name}%'").paginate(:page => 1, :per_page => 10).order('name DESC')
		render json: bars
	end
end
