class LoginController < ApplicationController

	require 'securerandom'

	def login
		begin  
			data = ActiveSupport::JSON.decode(request.body.string)
		rescue JSON::ParserError  
			return render text:"Unable to parse the data"
		end

		uname = data["username"].to_s
		pwd = data["password"].to_s

		if (user_in)
			return render text: "Already logged in"
		end

		user = User.where(:username => uname).first
		if user && user.try(:authenticate, pwd)

			token = SecureRandom.hex(32)
			puts token
			set_key(token, "#{user.id} #{user.confirmed}")

			user = user.to_json
			user = ActiveSupport::JSON.decode(user)
			user["token"] = token
			user.delete("password_digest")
			user.delete("created_at")
			user.delete("updated_at")

			render json: user
		else
			render text: "false"
		end

		return
	end

	def logout
		if (user_in)
			un_set(request.headers["Authorization"])
			render text: "true"
			return
		else
			render text: "Not loggedin"
			return
		end
	end

end
