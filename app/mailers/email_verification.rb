class EmailVerification < ActionMailer::Base
	default from: "from@example.com"
	
	def send_simple_message(key, _id, email, name)
		RestClient.post "https://api:key-fd109156f417660457877d53f5545adc"\
		"@api.mailgun.net/v3/sandboxe41da7ebf7dd42a3a19a5ac14dc6ec39.mailgun.org/messages",
		:from => "Chipmunk <postmaster@sandboxe41da7ebf7dd42a3a19a5ac14dc6ec39.mailgun.org>",
		:to => "#{name} <#{email}>",
		:subject => "Account Confirmation",
		:text => "Click the link to confirm your account.  http://localhost:8080/user/confirm/#{_id}/#{key}"
	end
end
