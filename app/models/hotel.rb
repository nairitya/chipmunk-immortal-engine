class Hotel < ActiveRecord::Base
	validates_presence_of :name, :is_restaurant, :is_bar, :is_hotel
	belongs_to :city, :foreign_key => :city_id
end
