class City < ActiveRecord::Base
	#has_one :county
	has_many :bars
	has_many :restaurants

	validates_presence_of :name
	belongs_to :country, :foreign_key => :country_id
end
