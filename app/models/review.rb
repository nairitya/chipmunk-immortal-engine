class Review < ActiveRecord::Base
	# has_one :category
	belongs_to :user, :foreign_key => :user_id
	# belongs_to :category, :foreign_key => :category_id

	belongs_to :hotel, :foreign_key => :type_id
	belongs_to :movie, :foreign_key => :type_id
end
