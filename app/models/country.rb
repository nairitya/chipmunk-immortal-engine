class Country < ActiveRecord::Base

	has_many :cities

	validates_presence_of :name
	validates_presence_of :code

	validates :name,
    :uniqueness => { :case_sensitive => false }

    validates :code,
    :uniqueness => { :case_sensitive => false }
end
