class Like < ActiveRecord::Base
	belongs_to :review, :foreign_key => :review_id
	belongs_to :user, :foreign_key => :user_id
end
