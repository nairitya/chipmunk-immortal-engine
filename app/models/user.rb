class User < ActiveRecord::Base

	has_many :friends
	has_many :reviews
	has_many :likes
	has_one :confirmation
	
	has_secure_password
	validates_presence_of :firstname
	validates_presence_of :lastname
	

	validates :email, :format => { :with => /\A[^@ ]+@[^@ ]+\.[^@ ]+\Z/ },
    :uniqueness => { :case_sensitive => false }

    validates :password, :presence => true, :on => :create

	validates :username,
    :format => { :with => /\A[A-Za-z0-9][A-Za-z0-9_-]{0,24}\Z/ },
    :uniqueness => { :case_sensitive => false }

	before_create :default_user_inactive

	def default_user_inactive
		#If you'll give this false, then it will not parse itselt to database campatible format.
		#Thus will be a format error, And give null error message. Thus making it very hard to find the error.
		#This var in boolean but is assigned 0 therefore.
		self.confirmed = 0
	end
end
